from telegram.ext import Updater
from telegram.ext import CommandHandler, MessageHandler, Filters
from telegram.message import Message
import markov as mkv
import logging
import random
import os

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
logger = logging.getLogger()
TOKEN = os.getenv("TOKEN")
t = 20
logger.info('Loading dictionary')
dic = mkv.get_dictionary('msgs', 'dict.json')
logger.info('Done')


def start(update, context):
    context.bot.send_message(
        chat_id=update.effective_chat.id, text="I'm a Markov's chain!")


def echo(update, context):

    def reply(msg):
        update.message.reply_text(msg, quote=True)
    go = True

    for e in update.message.parse_entities():
        if e['type'] == 'mention':
            reply(mkv.get_sentence(dic))
            go = False
    if go:
        sentence = update.message.text.split(' ')
        username = update.message.chat.username
        mkv.update_dictionary(dic, sentence)
        reply_to_msg = update.message.reply_to_message
        if isinstance(reply_to_msg, Message):
            last = sentence[-1]
            sentence = mkv.get_remain_sentence(dic, last).split(' ')
            sentence.pop(0)
            if len(sentence) > 0:
                reply(' '.join(sentence))
            else:
                reply(mkv.get_sentence(dic))
        elif random.randrange(t) <= 1:
            reply(mkv.get_sentence(dic))


if __name__ == '__main__':
    logger.info("Starting bot")
    updater = Updater(token=TOKEN, use_context=True)
    dispatcher = updater.dispatcher
    echo_handler = MessageHandler(Filters.text, echo)
    start_handler = CommandHandler('start', start)

    dispatcher.add_handler(echo_handler)
    dispatcher.add_handler(start_handler)
    updater.start_polling()
