# To add a new cell, type '# %%'
# To add a new markdown cell, type '# %% [markdown]'
# %%
from html.parser import HTMLParser
import codecs
import random
import collections
import os
import json

# %%


def update_dictionary(dict, str_array):
    length = len(str_array)
    for i, boh in enumerate(str_array):
        corr = str_array[i]
        if corr not in dict:
            dict[corr] = {}
        if i < length-1:
            next = str_array[i+1]
        else:
            next = 'EOL'
        if next in dict[corr]:
            dict[corr][next] = dict[corr][next] + 1
        else:
            dict[corr][next] = 1
    return dict

# %%


def get_dictionary(path, file_name):
    diz = {}

    class MyHTMLParser(HTMLParser):
        def __init__(self):
            HTMLParser.__init__(self)
            self.print = False

        def handle_starttag(self, tag, attrs):
            if tag == 'div':
                for name, value in attrs:
                    if name == 'class' and value == 'text':
                        self.print = True

        def handle_endtag(self, tag):
            if tag == 'div':
                self.print = False

        def handle_data(self, data):
            if self.print:
                list = data.strip().split(' ')
                if list[0] != '':
                    update_dictionary(diz, list)

    try:
        tmp = open(file_name, "r")
        diz = json.load(tmp)
    except:
        parser = MyHTMLParser()
        files = [os.path.join(path, f)
                 for f in os.listdir(path) if 'html' in f]

        for file in files:
            f = codecs.open(file, 'r')
            parser.feed(f.read())

        jsonDiz = json.dumps(diz)
        f = open(file_name, "w")
        f.write(jsonDiz)
        f.close()
    return diz


# %%
def get_remain_sentence(dict, sentence):
    next = sentence
    while True:
        freq = [dict[next][i] for i in dict[next].keys()]
        tmp = random.choices(list(dict[next]), weights=freq)
        next = tmp[0]
        if next != 'EOL':
            sentence = sentence + ' ' + next
        else:
            break
    return sentence

# %%


def get_sentence(dict):
    freq = [len(dict[i]) for i in dict.keys()]
    first = random.choices(list(dict), weights=freq)
    return get_remain_sentence(dict, first[0])
